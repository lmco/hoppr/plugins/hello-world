# Hoppr Starter Plugin

A basic Hoppr plugin, used in the [Starter Plugin Example](https://hoppr.dev/docs/development/extending/plugins/basic).

## Prerequisites

- Python 3.10 or higher
- [Poetry](https://python-poetry.org/docs/#installation)

## Usage

### Getting Started

- Clone this repository: `git clone https://gitlab.com/hoppr/plugins/starter-plugin.git`
- Change to the cloned repo: `cd starter-plugin`
- Install Hoppr and its dependencies: `poetry install`

### Basic

To see it in action, run Hoppr with this plugin enabled:

```shell
export ENVIRONMENT_VAR_NAME="my_password"  # referenced in starter_files/credentials.yml
hopctl bundle --transfer starter_files/transfer.yml --credentials starter_files/credentials.yml starter_files/manifest.yml
```

### With Attestation

To create an attestation of the hoppr bundle contents:

- Generate an in-toto project owner key: `in-toto-keygen project_owner_key`
- Generate an in-toto functionary key: `in-toto-keygen functionary_key`
- Generate an in-toto layout: `hopctl generate layout --transfer starter_files/transfer.yml --project-owner-key project_owner_key --functionary-key functionary_key`
- Run Hoppr with this plugin enabled using the `--attest` flag:

```shell
export ENVIRONMENT_VAR_NAME="my_password"  # referenced in starter_files/credentials.yml
hopctl bundle --transfer starter_files/transfer.yml --credentials starter_files/credentials.yml --attest --functionary-key functionary_key starter_files/manifest.yml
in-toto-verify --layout in-toto.layout --layout-keys project_owner_key.pub --verbose
```
